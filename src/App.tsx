import './App.css';
import Docs from './components/Docs';
import Tutorial from './components/Tutorials';
import Community from './components/Community';
import Menu from './components/Menu';

function App() {
  return (
    <div className="App">
      <Menu />
      <Docs />
      <Tutorial />
      <Community />
    </div>
  );
}

export default App;
